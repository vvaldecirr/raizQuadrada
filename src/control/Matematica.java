package control;

/**
 * Algoritmo de resolu��o de Raiz Quadrada
 */
public class Matematica {

	public static double raizQuadrada(double n) {
		double sup;
		double inf;		
		double meio;		
		
		sup = n;
		inf = 0;		
		
		do {
			meio = (inf+sup) / 2;
			
			if ((meio * meio) < n) 
				inf = meio;
			else
				sup = meio;
			
		} while ((sup - inf) > 0.00000001);
		
		return meio;
	}
	
}
